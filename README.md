# EINBLIQ.IO Firebolt API

This documentation describes the EINBLIQ.IO Firebolt API.

## Access

The base Url scheme for the API is:

    https://{customerId}-sa-api.1bliq.io


> NOTE: The `customerId` will be provided by team@einbliq.io
	
The `Swagger` documentation is available here:

    https://docs-sa-api.1bliq.io/doc

## Authentication/Authorization

Any client interacting with the APIs needs to provide an access token that has the privilege to access the resource which it is requesting.

> NOTE: The access token will be provided by team@einbliq.io

The API access token needs to be passed as an `Authorization` header in each HTTP request.

*Exampe of an HTTP authenticated request:*
```javascript
GET  https://{customerId}-sa-api.1bliq.io/{resource} HTTP/1.1
Authorization: Bearer YOUR-API-TOKEN
Accept: application/json
```

All APIs are based on the REST and JSON standard.

## HTTP Errors

The EINBLIQ.IO APIs return the following HTTP status codes, if the request is not valid:

|  Error  | Description |
|--|--|
| 400 | A malformed request.|
| 401|  Missing token or Authentication failed. |
| 404 | Resource not found. |
| 429 | Too many requests. |
| 500 | Server Error. |

Errors are returned in the following JSON format:

```javascript
{
	"code":  400,
	"message":  "bad request"
}
```

## Firebolt Endpoint

### POST - /v1/firebolt

The Firebolt endpoint takes an array of `Firebolt-API` objects, as well as the `appId`, from which the data is originating.

**Example Body Firebolt-API object**

```json
[
  {
    "appId": "string",
    "data": {
      "jsonrpc": "string",
      "method": "string",
      "id": "string",
      "params": {
        "prop1": "string",
        "prop2": "string",
        "prop3": "string",
        ...
        ...
      }
    }
  },
  ...
  ...
]
```

In case of a succesful request, the client receives an HTTP status code `200`.

### Validation

The API performs a formal validation on the incoming payload. In case any validation errors are detected, the API rejects the entire payload and returns a HTTP status code `400` or `422`.

The validation is performed on the following properties of each `Firebolt-API` object:

- `appId` not null or empty
- `data` not null 
